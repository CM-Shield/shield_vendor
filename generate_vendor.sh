#!/bin/bash

function patch_t124_glcore() {
  sed -i 's/libc.so/libs.so/g' nvidia/t124/nvgpu/lib/libglcore.so
  sed -i 's/vsnprintf/haxprintf/g' nvidia/t124/nvgpu/lib/libglcore.so
  sed -i 's/__haxprintf/__vsnprintf/g' nvidia/t124/nvgpu/lib/libglcore.so
  sed -i 's/libc.so/libs.so/g' nvidia/t124/nvgpu/lib64/libglcore.so
  sed -i 's/vsnprintf/haxprintf/g' nvidia/t124/nvgpu/lib64/libglcore.so
  sed -i 's/__haxprintf/__vsnprintf/g' nvidia/t124/nvgpu/lib64/libglcore.so
}

function patch_t124_scf() {
  sed -i 's/libprotobuf-cpp-lite.so/libprotobuf-cpp-lold.so/g' nvidia/t124/camera/lib/libscf.so
}

# The previous default crashes the current firmware
function patch_tx1_nvram() {
  sed -i 's/ccode=XR/ccode=XX/' nvidia/foster/bcm_firmware/bcm4354/nvram_jetsonE_cv_4354.txt
}

# Used by btlinux
function fetch_bcm4356_patchfile() {
  wget 'https://github.com/winterheart/broadcom-bt-firmware/raw/ddb24edc5169d064af3f405d6307aa4661a2cc52/brcm/BCM4356A2-13d3-3488.hcd' -O nvidia/common/bcm_firmware/bcm4356/BCM4356A2-13d3-3488.hcd
}

function patches() {
  patch_t124_glcore;
  patch_t124_scf;
  patch_tx1_nvram;
  fetch_bcm4356_patchfile;
}

mkdir -p downloads;
while read -r sname url version extra_path; do
  fileext=;
  if [ "${version}" == "git" ]; then
    fileext="sh";
  elif [ "$(echo ${version} |cut -d- -f1)" == "l4t" ]; then
    fileext="tbz2";
  else
    fileext="zip";
  fi;
  wget ${url} -O downloads/${sname}.${fileext};
  mkdir -p extract/${sname};
  if [ "${version}" == "git" ]; then
    tail -n +$(($(grep -an "^\s*__START_TGZ_FILE__" downloads/${sname}.sh | awk -F ':' '{print $1}') + 1)) downloads/${sname}.sh | tar zxv -C extract/${sname};
  elif [ "$(echo ${version} |cut -d- -f1)" == "l4t" ]; then
    mkdir -p extract/${sname}/drivers;
    tar -xf downloads/${sname}.tbz2 -C extract/${sname}
    tar -xf extract/${sname}/Linux_for_Tegra/nv_tegra/nvidia_drivers.tbz2 -C extract/${sname}/drivers
  else
    unzip -d extract/${sname} downloads/${sname}.zip;
    case "${sname}" in
      "stock-t114")
        mkdir -p extract/${sname}/temp;
        tail -n +$(($(grep -an "^\s*__START_TGZ_FILE__" extract/${sname}/extract-nv-recovery-image-shield-*.sh | awk -F ':' '{print $1}') + 1)) extract/${sname}/extract-nv-recovery-image-shield-*.sh | tar zxv -C extract/${sname}/temp;
        simg2img extract/${sname}/temp/system.img extract/${sname}/system.img;
        mkdir -p extract/${sname}/system;
        7z x -oextract/${sname}/system extract/${sname}/system.img;
	rm -rf extract/${sname}/temp extract/${sname}/extract-nv-recovery-image-shield-*.sh extract/${sname}/system.img;
        ;;
      "stock-t124")
        simg2img extract/${sname}/nv-recovery-st-wx-un-do-*/system.img extract/${sname}/system.img;
        mkdir -p extract/${sname}/system;
        7z x -oextract/${sname}/system extract/${sname}/system.img;
	rm -rf extract/${sname}/nv-recovery-st-wx-un-do-* extract/${sname}/system.img;
        ;;
      "stock-t210" | "stock-t210-rel24" | "stock-foster")
        simg2img extract/${sname}/nv-recovery-image-shield-atv-*/system.img extract/${sname}/system.img;
        simg2img extract/${sname}/nv-recovery-image-shield-atv-*/vendor.img extract/${sname}/vendor.img;
        mkdir -p extract/${sname}/system;
        mkdir -p extract/${sname}/vendor;
        7z x -oextract/${sname}/system extract/${sname}/system.img;
        7z x -oextract/${sname}/vendor extract/${sname}/vendor.img;
	rm -rf extract/${sname}/nv-recovery-image-shield-atv-* extract/${sname}/system.img extract/${sname}/vendor.img;
        ;;
      "stock-sif") # Seriously nvidia... You linked an ota as a recovery image...
	brotli -d extract/${sname}/system.new.dat.br
	brotli -d extract/${sname}/vendor.new.dat.br
	python2 tools/sdat2img.py extract/${sname}/system.transfer.list extract/${sname}/system.new.dat extract/${sname}/system.img
	python2 tools/sdat2img.py extract/${sname}/vendor.transfer.list extract/${sname}/vendor.new.dat extract/${sname}/vendor.img
        mkdir -p extract/${sname}/system;
        mkdir -p extract/${sname}/vendor;
        7z x -oextract/${sname}/system extract/${sname}/system.img;
        7z x -oextract/${sname}/vendor extract/${sname}/vendor.img;
	rm -rf extract/${sname}/system.* extract/${sname}/vendor.* extract/${sname}/boot.img extract/${sname}/blob extract/${sname}/bmp.blob extract/${sname}/compatibility.zip extract/${sname}/META-INF;
	;;
    esac;
  fi;

  while read -r fname source dest; do
    if [ "$sname" == "$fname" ]; then
      mkdir -p nvidia/$(dirname $dest);
      if [[ ${source} != "${source/bcmbinaries/}" ]]; then
        extrapath="prebuilt/t210"
      elif [[ ${source} != "${source/cypress/}" ]]; then
        extrapath="prebuilt/t210"
      elif [[ ${source} != "${source/model_frontal/}" ]]; then
        extrapath=""
      else
        extrapath="$extra_path"
      fi;
      cp extract/${sname}/${extrapath}/${source} nvidia/${dest};
    fi;
  done < file.list;
done < sources.txt;

find nvidia -path nvidia/common/tegraflash -prune -false -type f -exec chmod 644 {} \;

patches;

rm -f missing.list
while read -r fname source dest; do
  if [ -n "${dest}" -a ! -f "nvidia/${dest}" ]; then
    echo "${dest} not found";
    echo "${dest}" >> missing.list;
  fi;
done < file.list;
